from django.contrib import admin

from audit.models import Audit

admin.site.register(Audit)
