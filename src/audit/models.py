from django.db import models


class Audit(models.Model):
    user_id = models.CharField(max_length=100)
    action = models.CharField(max_length=100)
    time = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "audit"
