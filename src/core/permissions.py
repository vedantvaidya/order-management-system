from rest_framework import permissions


class GroupPermission(permissions.BasePermission):
    def has_permission(self, request, view):
        app_model = getattr(view, "app_model", None)
        required_permission = find_permission(app_model, request)
        # print(request.user.get_all_permissions())
        # print(required_permission)
        return request.user.has_perm(required_permission)


def find_permission(app_model, request):
    if request.method == "GET":
        return f"{app_model[0]}.view_{app_model[1]}"
    elif request.method == "POST":
        return f"{app_model[0]}.add_{app_model[1]}"
    elif request.method == "PUT":
        return f"{app_model[0]}.change_{app_model[1]}"
    elif request.method == "DELETE":
        return f"{app_model[0]}.delete_{app_model[1]}"
