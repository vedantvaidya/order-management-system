from django.contrib import admin
from django.urls import path, include
from drf_spectacular.views import (
    SpectacularAPIView,
    SpectacularRedocView,
    SpectacularSwaggerView,
)
from django.conf.urls.static import static
from django.conf import settings


apipatterns = [
    path("schema/", SpectacularAPIView.as_view(), name="schema"),
    path("docs/", SpectacularSwaggerView.as_view(url_name="schema"), name="swagger-ui"),
    path("redocs/", SpectacularRedocView.as_view(url_name="schema"), name="redoc"),
    path("user/", include("users.urls"), name="user"),
    path("product/", include("products.urls"), name="product"),
    path("order/", include("orders.urls"), name="order"),
]


urlpatterns = [
    path("admin/", admin.site.urls),
    path("api/", include(apipatterns)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    urlpatterns += [
        path("__debug__/", include("debug_toolbar.urls")),
    ]
