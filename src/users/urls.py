from django.urls import path
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
    TokenVerifyView,
)

from users import views

urlpatterns = [
    path("", views.UserListAPIView.as_view(), name="user-list-api"),
    path("login/", TokenObtainPairView.as_view(), name="token_obtain_pair"),
    path("token/refresh/", TokenRefreshView.as_view(), name="token_refresh"),
    path("token/verify/", TokenVerifyView.as_view(), name="token_verify"),
    path("register", views.UserRegistrationAPIView.as_view(), name="user-register-api"),
    path("my-account", views.UserAccountAPIView.as_view(), name="user-account-api"),
]
