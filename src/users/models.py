from django.contrib.auth.models import AbstractUser, BaseUserManager, PermissionsMixin
from django.db import models

from core.models import TimestampModel
from users.manager import UserManager


class User(AbstractUser, TimestampModel, PermissionsMixin):
    username = None
    email = models.EmailField(unique=True)
    address = models.TextField()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []
    objects = UserManager()
    all_objects = BaseUserManager()
