from users.models import User
from .test_setup import TestSetUp
from faker import Faker
import pdb
from django.contrib.auth.models import Group

faker = Faker()


class TestViews(TestSetUp):
    def test_user_cannot_register_without_data(self):
        response = self.client.post(self.register_url)
        self.assertEqual(response.status_code, 400)

    def test_user_cannot_register_with_simple_password(self):
        self.user_data["password"] = "123"
        response = self.client.post(self.register_url, self.user_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_user_cannot_register_with_existing_email(self):
        self.client.post(self.register_url, self.user_data, format="json")
        response = self.client.post(self.register_url, self.user_data, format="json")
        self.assertEqual(response.status_code, 400)

    def test_user_can_register(self):
        response = self.client.post(self.register_url, self.user_data, format="json")
        self.assertEqual(response.status_code, 201)
        self.assertEqual(response.data["first_name"], self.user_data["first_name"])
        self.assertEqual(response.data["last_name"], self.user_data["last_name"])
        self.assertIsNone(response.data.get("password"))
        self.assertEqual(response.data["address"], self.user_data["address"])

    def test_user_cannot_login_with_unregistered_email(self):
        self.client.post(self.register_url, self.user_data, format="json")
        self.user_data["email"] = faker.free_email()
        response = self.client.post(self.login_url, self.user_data, format="json")
        self.assertEqual(response.status_code, 401)

    def test_user_can_login_with_registered_email(self):
        self.client.post(self.register_url, self.user_data, format="json")
        response = self.client.post(self.login_url, self.user_data, format="json")
        self.assertEqual(response.status_code, 200)

    def test_user_can_see_his_details(self):
        self.client.post(self.register_url, self.user_data, format="json")
        login_response = self.client.post(self.login_url, self.user_data, format="json")
        headers = {'HTTP_AUTHORIZATION': f'Bearer {login_response.data["access"]}'}
        user_account_response = self.client.get(self.my_account_url, **headers)
        self.assertEqual(user_account_response.data[0]["first_name"], self.user_data["first_name"])
        self.assertEqual(user_account_response.data[0]["last_name"], self.user_data["last_name"])
        self.assertEqual(user_account_response.data[0]["email"], self.user_data["email"])
        self.assertEqual(user_account_response.data[0]["address"], self.user_data["address"])
        self.assertEqual(user_account_response.status_code,200)

    def test_admin_can_see_all_users_details(self):
        register_response=self.client.post(self.register_url, self.user_data, format="json")
        admin_group= Group.objects.get(name='Admin')
        user_instance=User.objects.get(id=register_response.data['id'])
        user_instance.groups.add(admin_group)
        login_response = self.client.post(self.login_url, self.user_data, format="json")
        headers = {'HTTP_AUTHORIZATION': f'Bearer {login_response.data["access"]}'}
        user_list_response = self.client.get(self.user_list_url, **headers)
        self.assertEqual(user_list_response.status_code,200)


