from rest_framework.test import APITestCase, APIClient
from django.urls import reverse
from faker import Faker
from django.contrib.auth.models import Group
from django.contrib.auth.models import Permission

faker = Faker()


class TestSetUp(APITestCase):

    
    
    def setUp(self):
        self.register_url = reverse("user-register-api")
        self.login_url = reverse("token_obtain_pair")
        self.my_account_url = reverse("user-account-api")
        self.user_list_url = reverse("user-list-api")
        set_permissions()
        
        
        
        self.user_data = {
            "first_name": faker.first_name(),
            "last_name": faker.last_name(),
            "email": faker.free_email(),
            "password": "Nimap@123",
            "address": faker.address(),
        }
        return super().setUp()

    def tearDown(self):
        return super().tearDown()


def set_permissions():
    admin_permissions()
    customer_permissions()   
    customer_support_permissions()   

def admin_permissions():
    admin_group = Group.objects.get(name='Admin')
    models=['user',"product","order","orderproduct","orderstatushistory","audit"]
    for model in models:
        admin_group.permissions.add(Permission.objects.get(codename=f'view_{model}'))
        admin_group.permissions.add(Permission.objects.get(codename=f'add_{model}'))
        admin_group.permissions.add(Permission.objects.get(codename=f'change_{model}'))
        admin_group.permissions.add(Permission.objects.get(codename=f'delete_{model}'))
    admin_group.save()
    # group_permissions = admin_group.permissions.all()
    # for permission in group_permissions:
    #     print(permission.name)

def customer_permissions():
    customer_group = Group.objects.get(name='Customer')
    customer_group.permissions.add(Permission.objects.get(codename='add_order'))
    customer_group.permissions.add(Permission.objects.get(codename='view_orderstatushistory'))
    customer_group.permissions.add(Permission.objects.get(codename='view_product'))


def customer_support_permissions():
    customer_support_group = Group.objects.get(name='Customer Support')
    models=["product","order","orderproduct","orderstatushistory"]
    for model in models:
        customer_support_group.permissions.add(Permission.objects.get(codename=f'view_{model}'))
        customer_support_group.permissions.add(Permission.objects.get(codename=f'add_{model}'))
        customer_support_group.permissions.add(Permission.objects.get(codename=f'change_{model}'))
        customer_support_group.permissions.add(Permission.objects.get(codename=f'delete_{model}'))
    customer_support_group.permissions.add(Permission.objects.get(codename='view_user'))
    customer_support_group.save()