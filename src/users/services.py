import re

from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import Group
from django.db import transaction
from rest_framework.exceptions import ValidationError

from users.models import User


@transaction.atomic
def register_user(data):
    password = data.pop("password")
    if validate_password(password):
        password = make_password(password)
        user = User.objects.create(
            password=password,
            **data,
        )
        group = Group.objects.get(name="Customer")
        user.groups.add(group)
        return user
    else:
        raise ValidationError("Invalid Password")


def validate_password(password):
    if len(password) < 8:
        return False
    if not re.search(r"[A-Z]", password):
        return False
    if not re.search(r'[!@#$%^&*(),.?":{}|<>]', password):
        return False
    if not re.search(r"\d", password):
        return False
    return True
