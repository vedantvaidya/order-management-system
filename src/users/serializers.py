from rest_framework import serializers

from users import services
from users.models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            "id",
            "first_name",
            "last_name",
            "email",
            "password",
            "address",
        )
        extra_kwargs = {
            "password": {"write_only": True},
        }

        read_only_fields = ["id"]

    def create(self, validated_data):
        return services.register_user(validated_data)
