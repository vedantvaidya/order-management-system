from django.conf import settings
from django.shortcuts import render
from rest_framework.generics import CreateAPIView, ListAPIView

from core.permissions import GroupPermission
from users import serializers
from users.models import User
from rest_framework.permissions import IsAuthenticated

class UserRegistrationAPIView(CreateAPIView):
    serializer_class = serializers.UserSerializer
    permission_classes = []


class UserListAPIView(ListAPIView):
    serializer_class = serializers.UserSerializer
    queryset = User.objects.all()
    permission_classes = [GroupPermission]
    app_model = ["users", "user"]


class UserAccountAPIView(ListAPIView):
    serializer_class=serializers.UserSerializer
    permission_classes=[IsAuthenticated]
    def get_queryset(self):
        queryset=User.objects.filter(email=self.request.user)
        return queryset