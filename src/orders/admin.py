from django.contrib import admin

from .models import Order, OrderProduct, OrderStatusHistory

admin.site.register(Order)
admin.site.register(OrderProduct)


class OrderStatusHistoryAdmin(admin.ModelAdmin):
    list_display = ("order", "time", "status_id")


admin.site.register(OrderStatusHistory, OrderStatusHistoryAdmin)
