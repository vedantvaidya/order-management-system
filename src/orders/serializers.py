from rest_framework import serializers
from rest_framework.serializers import ValidationError

from orders.services import place_order
from products.models import Product
from users.serializers import UserSerializer

from .models import Order, OrderProduct, OrderStatusHistory, StatusChoice


class OrderProductCreateSerializer(serializers.Serializer):
    product = serializers.IntegerField()
    quantity = serializers.IntegerField()


class PlaceOrderSerializer(serializers.Serializer):
    cart = OrderProductCreateSerializer(many=True)

    def create(self, validated_data):
        return place_order(validated_data,self.context["request"].user)


class UserEmailSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    email = serializers.CharField()


class ProductTitleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = ("title",)


class OrderProductListSerializer(serializers.ModelSerializer):
    product = ProductTitleSerializer()

    class Meta:
        model = OrderProduct
        fields = [
            "id",
            "product",
            "quantity",
        ]


class OrderSerializer(serializers.ModelSerializer):
    orderproduct_set = OrderProductListSerializer(many=True)
    user = UserEmailSerializer()

    class Meta:
        model = Order
        fields = (
            "id",
            "user",
            "created_at",
            "updated_at",
            "status",
            "orderproduct_set",
        )


class StatusChoiceSerializer(serializers.Serializer):
    id = serializers.IntegerField()
    label = serializers.CharField()


class OrderRetriveSerializer(serializers.ModelSerializer):
    orderproduct_set = OrderProductListSerializer(many=True)
    user = UserSerializer()

    class Meta:
        model = Order
        fields = (
            "id",
            "user",
            "created_at",
            "updated_at",
            "status",
            "orderproduct_set",
        )


class OrderStatusUpdateSerializer(serializers.Serializer):
    status_id = serializers.IntegerField()

    def update(self, instance, validated_data):
        choices = [i[0] for i in StatusChoice.choices]
        if validated_data["status_id"] not in choices:
            raise ValidationError({"detail": "Invalid Input"})
        instance.status_id = validated_data["status_id"]
        instance.save()
        OrderStatusHistory.objects.create(
            order_id=instance.id, status_id=validated_data["status_id"]
        )
        return instance


class OrderStatusHistorySerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderStatusHistory
        fields = (
            "time",
            "status",
        )
