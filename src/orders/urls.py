from django.urls import path
from . import views


urlpatterns = [
    path("", views.OrderListAPIView.as_view(), name="order-list-api"),
    path("<int:pk>", views.OrderRetriveAPIView.as_view(), name="order-retrive-api"),
    path(
        "<int:id>/status-history",
        views.OrderStatusHistoryRetriveAPIView.as_view(),
        name="order-status-history-retrive-api",
    ),
    path(
        "order-history", views.OrderHistoryAPIView.as_view(), name="order-history-api"
    ),
    path("place-order", views.PlaceOrderAPIView.as_view(), name="place-order-api"),
    path("status-list", views.StatusChoiceListView.as_view(), name="status-list-api"),
    path(
        "status-update/<int:pk>",
        views.StatusChoiceUpdateView.as_view(),
        name="status-update-api",
    ),
]
