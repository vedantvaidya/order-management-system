from django.db.models import Prefetch
from django.shortcuts import render
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination
from rest_framework.permissions import DjangoModelPermissions, IsAuthenticated

from core.permissions import GroupPermission

from .models import Order, OrderProduct, OrderStatusHistory, StatusChoice
from .serializers import (
    OrderRetriveSerializer,
    OrderSerializer,
    OrderStatusHistorySerializer,
    OrderStatusUpdateSerializer,
    PlaceOrderSerializer,
    StatusChoiceSerializer,
)


class PlaceOrderAPIView(generics.CreateAPIView):
    serializer_class = PlaceOrderSerializer
    permission_classes = [GroupPermission]
    app_model = ["orders", "order"]


class OrderListAPIView(generics.ListAPIView):
    queryset = Order.objects.select_related("user").prefetch_related(
        Prefetch("orderproduct_set", OrderProduct.objects.select_related("product"))
    )
    serializer_class = OrderSerializer
    pagination_class = PageNumberPagination
    permission_classes = [GroupPermission]
    app_model = ["orders", "order"]


class OrderRetriveAPIView(generics.RetrieveAPIView):
    serializer_class = OrderRetriveSerializer
    permission_classes = [DjangoModelPermissions]
    queryset = Order.objects.select_related("user").prefetch_related(
        Prefetch("orderproduct_set", OrderProduct.objects.select_related("product"))
    )
    permission_classes = [GroupPermission]
    app_model = ["orders", "order"]


class StatusChoiceListView(generics.ListAPIView):
    serializer_class = StatusChoiceSerializer
    permission_classes = []

    def get_queryset(self):
        choices = [
            {"id": choice[0], "label": choice[1]} for choice in StatusChoice.choices
        ]
        return choices


class StatusChoiceUpdateView(generics.UpdateAPIView):
    queryset = Order.objects.all()
    serializer_class = OrderStatusUpdateSerializer
    permission_classes = [GroupPermission]
    app_model = ["orders", "order"]


class OrderHistoryAPIView(generics.ListAPIView):
    serializer_class = OrderSerializer
    pagination_class = PageNumberPagination
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        queryset = (
            Order.objects.filter(user_id=self.request.user.id)
            .select_related("user")
            .prefetch_related(
                Prefetch(
                    "orderproduct_set", OrderProduct.objects.select_related("product")
                )
            )
        )
        return queryset


class OrderStatusHistoryRetriveAPIView(generics.ListAPIView):
    serializer_class = OrderStatusHistorySerializer
    permission_classes = [GroupPermission]
    app_model = ["orders", "orderstatushistory"]

    def get_queryset(self):
        queryset = OrderStatusHistory.objects.filter(
            order_id=self.kwargs["id"],
        ).order_by("time")
        return queryset

    @method_decorator(cache_page(60))  # 5 minutes cache (60 seconds * 5)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)
