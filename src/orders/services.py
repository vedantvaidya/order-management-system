from django.db import transaction
from django.http import HttpResponse
from rest_framework.serializers import ValidationError

from audit.models import Audit
from products.models import Product

from .models import Order, OrderProduct, OrderStatusHistory


@transaction.atomic
def place_order(data,user):
    validate_quantity(data["cart"])
    order_instance = Order.objects.create(user_id=user.id)
    order_product_list = []
    for i in data["cart"]:
        order_product_instance = OrderProduct(
            order_id=order_instance.id, product_id=i["product"], quantity=i["quantity"]
        )
        order_product_list.append(order_product_instance)
    OrderProduct.objects.bulk_create(order_product_list)
    OrderStatusHistory.objects.create(order_id=order_instance.id, status_id=1)
    order_audit = Audit(user_id=user.id, action="Created Order")
    order_status_history_audit = Audit(
        user_id=user.id, action="Created OrderStatusHistory"
    )
    product_audit = Audit(user_id=user.id, action="Updates Product")
    Audit.objects.bulk_create([order_audit, order_status_history_audit, product_audit])

    return data


@transaction.atomic
def validate_quantity(data):
    products_id = []
    for i in data:
        products_id.append(i["product"])
    products = Product.objects.filter(id__in=products_id).order_by("id")
    products_id = sorted(products_id)
    data = sorted(data, key=lambda x: x["product"])
    if len(products_id) == len(products):
        for i in range(len(products)):
            if (data[i]["product"] == products[i].id) and (
                data[i]["quantity"] <= products[i].quantity_available
            ):
                products[i].quantity_available -= data[i]["quantity"]
                continue
            else:
                raise ValidationError({"detail": "Invalid Input"})
        products = Product.objects.bulk_update(products, fields=["quantity_available"])
        return products
    raise ValidationError({"detail": "Invalid Input"})
