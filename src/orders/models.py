from django.db import models
from core.models import TimestampModel

from users.models import User
from products.models import Product


class StatusChoice(models.IntegerChoices):
    order_placed = 1, "Order Placed"
    out_to_delivery = 2, "Out to Delivery"
    delivered = 3, "Delivered"



class Order(TimestampModel):
    user=models.ForeignKey(User, on_delete=models.CASCADE)
    status_id = models.SmallIntegerField(
        db_column="status",
        choices=StatusChoice.choices,
        default=StatusChoice.order_placed,
    )

    @property
    def status(self):
        return self.get_status_id_display()


    class Meta:
        db_table = "order"



class OrderProduct(models.Model):
    order=models.ForeignKey(Order,on_delete=models.CASCADE)
    product=models.ForeignKey(Product,on_delete=models.CASCADE)
    quantity=models.IntegerField()

    class Meta:
        db_table = "order_products"


class OrderStatusHistory(models.Model):
    order=models.ForeignKey(Order,on_delete=models.CASCADE)
    time=models.DateTimeField(auto_now=True)
    status_id = models.SmallIntegerField(
        db_column="status",
        choices=StatusChoice.choices,
        default=StatusChoice.order_placed,
    )
    @property
    def status(self):
        return self.get_status_id_display()
    
    class Meta:
        db_table = "order_status_history"