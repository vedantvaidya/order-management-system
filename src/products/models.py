from django.db import models


class CategoryChoice(models.IntegerChoices):
    food = 1, "Food"
    decorations = 2, "Decorations"
    electronics = 3, "Electronics"
    clothing = 4, "Clothing"
    sports = 5, "Sports"
    others = 6, "Others"


class Product(models.Model):
    title = models.CharField(max_length=50)
    category_id = models.SmallIntegerField(
        db_column="category",
        choices=CategoryChoice.choices,
        default=CategoryChoice.others,
    )
    price = models.IntegerField()
    description = models.TextField()
    quantity_available = models.IntegerField()
    image = models.ImageField(upload_to="product_images/", null=True, blank=True)

    @property
    def category(self):
        return self.get_category_id_display()

    def __str__(self):
        return self.title

    class Meta:
        db_table = "product"
