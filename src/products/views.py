from django.shortcuts import render
from rest_framework import generics
from rest_framework.pagination import PageNumberPagination

from .models import Product
from .serializers import ProductListAPISerializer


class ProductListAPIView(generics.ListAPIView):
    queryset = Product.objects.all()
    serializer_class = ProductListAPISerializer
    permission_classes = []
    pagination_class = PageNumberPagination

    def get_queryset(self):
        queryset = Product.objects.filter(quantity_available__gt=0)
        return queryset
