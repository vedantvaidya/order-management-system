# Generated by Django 4.2.4 on 2023-09-01 06:48

from django.db import migrations


class Migration(migrations.Migration):
    dependencies = [
        ("products", "0002_product_image"),
    ]

    operations = [
        migrations.RenameField(
            model_name="product",
            old_name="quantit_available",
            new_name="quantity_available",
        ),
    ]
