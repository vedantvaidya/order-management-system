# Order Management System

## Introduction
The Order Management System project serves as a practical exercise for a Django developer to showcase their expertise in API development, database design, error handling, logging, and Redis integration. This project involves creating a comprehensive API-driven system for managing customer orders, from creation to fulfillment.


## Key Features:

**User Authentication and Roles:**

Implement a user authentication system with roles such as admin, customer support, and regular users(customers). Admins can manage orders and products, while customers can place and view orders.

### Customers:
Customers can sign up using their email address and a password. Passwords should not be stored in plain text, it can be stored as a hash. Password validation should be done using RegEx with below conditions:
* Minimum length should be 8 characters
* 1 capital letter
* 1 special character
* 1 numeric character


**Order Creation and Tracking:**

* Create API endpoints to enable customers to place new orders, providing order details like products, quantities, and shipping information.

* Implement tracking functionality, allowing users to check the status and history of their orders.

**Inventory Management:**

* Design an inventory system where product availability is updated upon order placement and fulfillment.

* Ensure that API calls to purchase products update inventory levels and prevent overselling.

**Order Fulfillment Workflow:**

* Develop an order fulfillment process that involves marking orders as processed, shipped, and delivered.

* Once the order is delivered the status cannot be changed.

* Implement API endpoints for updating order statuses at different stages.

**Database Design:**

* Design a robust database schema that includes tables for users, products, orders, order items, and order status history.

* Define relationships between entities, such as the connection between users and their orders.

**Error Handling and Validation:**

* Implement thorough input validation and error handling to maintain data consistency and integrity.

* Provide clear error messages and appropriate HTTP status codes in response to invalid requests.

**Logging and Auditing:**

* Set up logging mechanisms to record critical events, such as order creation, updates, and inventory changes.

* Implement an audit trail to track modifications to orders and product inventory over time.

**Redis Caching for Order Status:**

* Utilize Redis to cache order status updates, ensuring quick retrieval and reducing database load.

* Implement caching strategies to store frequently accessed order statuses.


**API Documentation:**

* Generate detailed API documentation using tools like Swagger or Django REST framework's built-in capabilities.

* Document endpoints, request parameters, response formats, and authentication requirements.





### Tech Stack
| Tools | Version | Reference| Guide |
| ---  | ---     | ---      | --- |
| Python  | 3.11     | https://www.python.org/downloads/release/python-3110 |
| Django  | 4.2.4     | https://www.djangoproject.com  |
| PostgreSQL  | >=11.5     | https://www.postgresql.org/download | [Configuration Guide](/doc/config/database.md) |
| Redis  | -     | https://redis.io | [Installation Guide](/doc/config/redis.md) |

### Getting Started

* **Prerequisite**:  Make sure to Install above mentioned tools on you machine and follow configuration guide.
* Git clone this repository to your local machine.
* Go to cloned directory and create **.env** at root, copy all content from **.env.example** and fill in values for all environment variables
* Create a virtual env with name **".venv"** Refer, [Virtualenv Guide](https://www.geeksforgeeks.org/python-virtual-environment/)
* Change your working directory to **backend** with following command: 

    ```cd src```
* Install required packages from "requirements.txt" with following command:
    
    ```pip install -r requirements/base.txt```
    
    ```pip install -r requirements/dev.txt```
* Once done, Perform migrations with command: 
    
    ```python manage.py migrate``` 

* Once done, Create a super-user with command: 
    
    ```python manage.py createsuperuser```

* Set the proper email address and password. 
    
* Then you can run your project with following command: 
    
    ```python manage.py runserver```

* To set Permissions, you can go to admin pannel by going to the following path:

    ```localhost:8000/admin```
* Click on groups to see the 3 roles.
    * Admin
    * Customer-Support
    * Customer

* Click on the roles and you can set Permissions accordingly.